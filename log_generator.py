import datetime
from enum import Enum
from random import randint

class Severity(Enum):
    INFO = 0
    WARN = 1
    ERROR = 2
    CRITICAL = 3

class CellState(Enum):
    OK = 0
    FAIL = 1

def severity_to_string(severity):
    match severity:
        case Severity.INFO:
            return "INFO"
        case Severity.WARN:
            return "WARN"
        case Severity.ERROR:
            return "ERROR"
        case Severity.CRITICAL:
            return "CRITICAL"

def cell_state_to_string(cell_state):
    match cell_state:
        case CellState.OK:
            return "OK"
        case CellState.FAIL:
            return "FAIL"

def get_severity():
    value = randint(0, 1000000)
    if value < 1:
        return Severity.CRITICAL
    elif value < 3:
        return Severity.ERROR
    elif value < 20:
        return Severity.WARN
    else:
        return Severity.INFO

def get_cell_state(cell_state):
    value = randint(0, 100000)
    if value < 1:
        if cell_state == CellState.OK:
            return CellState.FAIL
        else:
            return CellState.OK
    else:
        return cell_state

event_time = datetime.datetime.now()
num_lines = randint(1, 1E7)
num_cells = randint(1, 100)
cell_states = {}
for cell in range(num_cells):
    cell_states[cell] = CellState.OK

with open ('lander.log', 'w') as log_file:
    for line in range(num_lines):
        event_time += datetime.timedelta(milliseconds = randint(1, 36000))
        cell_num = randint(0, num_cells - 1)
        severity = get_severity()
        cell_state = get_cell_state(cell_states[cell_num])
        cell_states[cell_num] = cell_state
        log_file.write(f"{event_time.isoformat()} {severity_to_string(severity)} Cell {cell_num + 1} Diagnostic {cell_state_to_string(cell_state)} <message>\n")

