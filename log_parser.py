'''
Questions:
    Can cells change state from FAIL to OK?
    How many cells are there?
'''

import os

CELL_NUM = 3
CELL_STATUS = 5

# rover file path for USB is "rover/media" I belive
# SEARCH_DIR = "C:\\Users\\mgwin\\Documents"
# SEARCH_DIR = "/home/quinn"
SEARCH_DIR = "/media"
LOG_FILE_NAME = "lander.log"

def parse_log_file(log_file_path):
    try:
        with open(log_file_path) as log_file:
            print("Parsing log file")
            malfunctioning_cells = set(())
            for line in log_file:
                line_components = line.split(" ")
                match line_components[CELL_STATUS]:
                    case "FAIL":
                        malfunctioning_cells.add(line_components[CELL_NUM])
                    case "OK":
                        malfunctioning_cells.discard(line_components[CELL_NUM])
                    case _:
                        print(f"Unknown status: {line_components[CELL_STATUS]}")
            return malfunctioning_cells
    except FileNotFoundError:
        print(f"The file {log_file_path} was not found.")
    except IOError as e:
        print(f"Error reading the file: {e}")

def crawl_folder(path):
    print(f"Searching for {LOG_FILE_NAME} in {path}...")
    try:
        for root, dirs, file_names in os.walk(path):
            for file_name in file_names:
                if file_name == LOG_FILE_NAME:
                    print(f"Found {file_name} in {root}")
                    return os.path.join(root, file_name)
    except NotADirectoryError:     
        print("Error: This is an invalid directory")
    except OSError:
        print("Error: System related error such as file not found or disk full")
    
if __name__ == "__main__":
    log_file_path = crawl_folder(SEARCH_DIR)
    malfunctioning_cells = parse_log_file(log_file_path)
    print(f"Malfunctioning Cells:\n{malfunctioning_cells}")
